function [p,id,count] = dup_check(p,x,count,tol)
%Check whether x is already exist in first count pts or not
%Input
%p: list of exist points, [countx2;zeros]
%x: coordinates of point to be checked, 1x2
%count: length of unique pts
%tol: tolerance
%Output
%p: update p if x is not duplicated
%id: return id of x, if x is duplicated, return id in p;
%count: if x is not duplicated, return count+1

dup_status = false;%duplication status
for k = 1:count %check duplication
    if norm(p(k,:)-x)<tol %duplicated point
        dup_status = true;
        id = k;% x is duplicated, return id in p
        break
    end
end
if ~dup_status %not a duplicated point
    count = count+1;%update count
    p(count,:) = x;%add point to p
    id = count;%return new id
end
end