%using triangulation class
classdef Streamline
    properties
        x %coordinates of intersection points
        status %SL exists or not
        d0 %starting direction, stored as location vector
        end_type %type of end point, -1=spiral 0=boundary N=singularity point id
        end_edge %store end edge if ends on boundary, point ids
    end
    methods
        function SL = Streamline %construct SL
            SL.x = [];
            SL.status = false;
            SL.d0 = [];
            SL.end_type = -1; %-1:spiral 0:boundary N,i:singularity
        end
        function SL = initializeSL(SL,S,TR,N) %initialize SL
            nmax = max([S.k])+4; %max num of SL emitting from S
            SL(size(S,2),nmax) = Streamline; %generate objective array
            for i = 1:size(S,2) %loop through S_i
                theta = theta0iter(S(i),TR,N); %find starting directions of S_i
                for j = 1:S(i).k+4 %loop through SLs around S_i
                    SL(i,j).status = true; %SL exists
                    SL(i,j).x(1,1) = S(i).x; %x0,y0
                    SL(i,j).x(1,2) = S(i).y; %each row stores location of x_i
                    SL(i,j).d0 = [cos(theta(j)) sin(theta(j))]; %starting direction
                end
            end
        end
    end
end


