function Snew = collide_S(S,d0,fh)
%Collide close singularities with opposite k 
%INPUT: 
%S: singularities
%d0: grid size
%OUTPUT:
%Snew: singularities after collision

for i = 1:size(S,2) %loop through S
    if S(i).k>-4 %exist
    for j = 1:size(S,2)
        if j>i && S(j).k>-4 && (S(i).k+S(j).k==0) %opposite k
            d = sqrt((S(i).x-S(j).x)^2+(S(i).y-S(j).y)^2);
            if d<d0*fh([S(i).x S(i).y]) %within distance d0 at S(i)
                S(i).k = -4; %remove
                S(j).k = -4;
            end
        end
    end
    end
end
Snew = Singularity; %record existing S
for i = 1:size(S,2)
    if S(i).k>-4 %exist
    Snew(end+1) = S(i);
    end
end
Snew = Snew(2:end); %remove first one
end