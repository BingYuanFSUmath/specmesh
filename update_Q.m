function Q = update_Q(Q,p,eid,pid,dup)
%update Q if Q(eid).P(pid) have been updated
%INPUT:
%Q: quad class
%p: new pt location
%eid: element id
%pid: pt id
%dup: duplicated pts
%OUTPUT:
%Q: updated Q

%update Q(eid).P(qid)
Q(eid).P(pid,:) = p;
%update dup pts
if size(dup{eid,pid},1)>0 %have duplicated pts
    %update all
    for id = 1:size(dup{eid,pid},1)
            Q(dup{eid,pid}(id,1)).P(dup{eid,pid}(id,2),:) = p;
    end
end