function status = member_check_row(a,b,tol)
%check whether a is a row element in b
%INPUT: 
%a: row vector, 1xn
%b: matrix, mxn
%tol: error tolerance
%OUTPUT:
%status: 0-is a member, 1-not a member

sizea = size(a,2);
sizeb = size(b,2);
if sizeb == 0 %b is empty
    status = 0; 
elseif sizea ~= sizeb
    error('a and b must have compatiable size')
else
    status = 0;
    for i = 1:size(b,1)
        max_e = 0; %max error
        for j = 1:sizea
            e = abs(a(j)-b(i,j)); %error
            if e>max_e
                max_e = e;
            end
        end
        if max_e<tol
            status = 1;
            break
        end
    end
end
