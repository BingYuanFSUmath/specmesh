function T = ellipsecurve_t(p,a,b,cx,cy,varargin)
%curve function of ellipse
%given point location on curve, return global parameter T
%INPUT:
%p: [x y], point coordinates on curve
%a/b: length of axis, x^2/a^2+y^2/b^2=1
%cx,cy: center coordinates
%ta,tb: angle range of ellipse
%OUTPUT:
%T: parameter, [0,1], counterclockwise

T = -1; 
tol = 1e-5;
x = p(1)-cx;
y = p(2)-cy;
if (isempty(varargin))
    ta = 0;
    dt = 2*pi;
else
    ta = varargin{1};
    dt = varargin{2};
end
if abs(x^2/a^2+y^2/b^2-1)<tol %on the ellipse
    theta = atan2(y/b,x/a);%[-pi/pi]
    if theta>=0
        T = theta-ta/dt;
    else
        T = (theta+2*pi)-ta/dt;
    end
end
T = T/(2*pi);
if (T>1 || T<0)
    T = -1;
end
    