function p = piecewise_linear(x,t)
%parametric function for piecewise linear curve
%x is not unifom
%t represents curve length ratio
%INPUT:
%x: curve nodes
%t: parameter
%OUTPUT:
%p: pt coordinates

N = size(x,1); %num of nodes
d = zeros(N,1);
l = 0; %total length
for i = 2:N
    d(i) = norm(x(i,:)-x(i-1,:));
    l = l+d(i);
    d(i) = l;
end
for i = 1:N-1
    if d(i)/l<=t && t<= d(i+1)/l
        s = (t*l-d(i))/norm(x(i+1,:)-x(i,:));
        p = x(i,:)*(1-s)+x(i+1,:)*s;
        break
    end
end


