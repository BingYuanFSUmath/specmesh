%example class
classdef Example
    properties
        id %example id
        curve_num %number of curves
        curve %pCurve class
        d0 %min grid size
        fd %distance function
        fh %local size function
        box %confine box
    end
    methods
        function ex = Example(id,curve_num)
            ex.id = id;
            ex.curve_num = curve_num;
            curve_name = cell(1,curve_num);
            curve(curve_num) = pCurve; %1xN pCurve
            for i = 1:curve_num
                curve_name{i} = ['curve' num2str(id) '_' num2str(i)];
                curve(i) = init_pCurve(curve(i),i,curve_name{i});
            end
            ex.curve = curve;
        end
    end
end