%find starting directions of SLs around S
%using triangulation class
function theta = theta0iter(S,TR,N) %S represents S_i
%randomly choose 2 vertices
id = zeros(1,3);
x = zeros(1,3);
y = zeros(1,3);
theta = zeros(1,3);
for i = 1:3
    id(i) = TR.ConnectivityList(S.E_ID,i); %vertexi ID
    x(i) = N(id(i)).x; %coordinates of v1
    y(i) = N(id(i)).y;
    theta(i) = N(id(i)).cross;
end
x0 = S.x; %coordinates of S_i
y0 = S.y;

Bisection = @(t,i,j,theta2) tan(t*theta(i)+(1-t)*theta2)...
    *(t*x(i)+(1-t)*x(j)-x0)-(t*y(i)+(1-t)*y(j)-y0);
for i = 1:3
    if i==3 
        j=1;%31
    else
        j=i+1;%12,23
    end
    theta1 = theta(i);
    theta2 = theta(j);
    dtheta = theta2-theta1;
    if dtheta>pi/4
        dtheta = dtheta-pi/2;
    elseif dtheta<-pi/4
        dtheta = dtheta+pi/2;
    end
    theta2 = theta1+dtheta;
    fa = Bisection(0,i,j,theta2);
    fb = Bisection(1,i,j,theta2);
    if fa*fb<0 %at least one soln
        break %record i,j,theta2
    end
end
a = 0;
b = 1;
d = abs(fa);
if d<1e-4 || d==1e-4
    c = a; %a is solution
end
while d>1e-4
    c = 0.5*(a+b);
    fc = Bisection(c,i,j,theta2);
    if fa*fc<0
        b = c;
    else
        a = c;
    end
    fa = Bisection(a,i,j,theta2);
    %fb = Bisection(b,i,j,theta2);
    d = abs(fc); %TEST--OK!
end

x = c*x(i)+(1-c)*x(j);%intersection coordinates
y = c*y(i)+(1-c)*y(j);
theta0 = atan2(y-y0,x-x0);
n = S.k+4; %num of SLs
theta = zeros(1,n);
for i = 1:n
    theta(i) = theta0+(i-1)*2*pi/n; 
end

end