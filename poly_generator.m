function poly_generator(id,i,pt)
%generate polygon curve files
%INPUT:
%id: example id
%i: curve number
%pt: polygon vertices
%OUTPUT:
%curve functions: p/t/y

pt = mat2str(pt);
%p file
name = ['curve',num2str(id),'_',num2str(i),'_p'];
fileID = fopen([name,'.m'],'w');
content = ['function p = ',name,'(t)\n'...
    ' pt = ',pt,';\n p = polycurve_p(t,pt);'];
fprintf(fileID,content);
fclose(fileID);
%t file
name = ['curve',num2str(id),'_',num2str(i),'_t'];
fileID = fopen([name,'.m'],'w');
content = ['function t = ',name,'(p)\n'...
    ' pt = ',pt,';\n t = polycurve_t(p,pt);'];
fprintf(fileID,content);
fclose(fileID);
%y file
name = ['curve',num2str(id),'_',num2str(i),'_y'];
fileID = fopen([name,'.m'],'w');
content = ['function y = ',name,'(x)\n'...
    ' pt = ',pt,';\n y = polycurve_y(x,pt);'];
fprintf(fileID,content);
fclose(fileID);