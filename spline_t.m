function T = spline_t(p,Ax,Ay,x)
%given pt and spline coefs, find parameter T
%INPUT:
%p: [x,y], pt coordinates
%Ax,Ay: spline coefficients
%x: coordinates of spline sample points, Nx2
%OUTPUT:
%T: parameter

tol = 1e-8; %p is not accurate because of inaccurate distance function
T = -1;
N = size(x,1); %num of pts
for i = 1:N-1
    xmax = max(x(i,1),x(i+1,1));
    xmin = min(x(i,1),x(i+1,1));
    if xmin<=p(1) && p(1)<=xmax && T<0
        %find t
        func = @(t) Ax(i,1)+Ax(i,2)*(t-i)+Ax(i,3)*(t-i)^2+...
            Ax(i,4)*(t-i)^3-p(1);
        t = bisection(func,[i,i+1],1e-10);
        t = (t-1)/(N-1);
        yt = spline_p(Ay,t);
        if abs(p(2)-yt)<tol
            T = t;
            break
        end
    end
end
if T<0 %didn't find t in x, check y
    for i = 1:N-1
        ymax = max(x(i,2),x(i+1,2));
        ymin = min(x(i,2),x(i+1,2));
        if ymin<=p(2) && p(2)<=ymax && T<0
            %find t
            func = @(t) Ay(i,1)+Ay(i,2)*(t-i)+Ay(i,3)*(t-i)^2+...
                Ay(i,4)*(t-i)^3-p(2);
            t = bisection(func,[i,i+1],1e-10);
            t = (t-1)/(N-1);
            xt = spline_p(Ax,t);
            if abs(p(1)-xt)<tol
                T = t;
                break
            end
        end
    end
end

    
    