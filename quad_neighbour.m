function [nei_ps,nei_qs,dup] = quad_neighbour(Qnew,Q,Ne,N)
nei_ps = cell(Ne,N^2); %record all nei_pts, Nx2, [eid local_pid]
dup = cell(Ne,N^2); %record repeated pts
for eid = 1:Ne %each quad
    p = Qnew(eid).P;
    %loop through each point
    for j = 1:N
        for i = 1:N
            pid = i+(j-1)*N; %local pt id
            nei_ps{eid,pid} = [ones(size(Qnew(eid).C{pid}))*eid,Qnew(eid).C{pid}];
            dup{eid,pid} = [];
            %local neighbour pt ids, Nx2, [eid local_pid]
            if i==1||i==N||j==1||j==N %bdy pts
                %find other neighbour pts
                if i==1 && j==1%p1
                    e = Q.E(Q.C{Q.Q(eid,1)},:);
                elseif i==N && j==1%p2
                    e = Q.E(Q.C{Q.Q(eid,2)},:);
                elseif i==N && j==N%p3
                    e = Q.E(Q.C{Q.Q(eid,3)},:);
                elseif i==1 && j==N%p4
                    e = Q.E(Q.C{Q.Q(eid,4)},:);
                else %local bdy pts
                    if j == 1 %on p1p2
                        e = [Q.Q(eid,1),Q.Q(eid,2)];
                        %edge, store global pt ids
                    end
                    if i == N %on p2p3
                        e = [Q.Q(eid,2),Q.Q(eid,3)];
                    end
                    if j == N %on p3p4
                        e = [Q.Q(eid,3),Q.Q(eid,4)];
                    end
                    if i == 1 %on p4p1
                        e = [Q.Q(eid,4),Q.Q(eid,1)];
                    end
                end
                for ne = 1:size(e,1)
                    for id = 1:Ne
                        quadid = 0; %neighbour quad id
                        if id ~= eid %exclude itself
                        if ~ismember(id,nei_ps{eid,pid}(:,1))
                            for k = 1:4
                                Qe = Q.E(Q.QE(id,k),:); %edge on quads
                                if ((e(ne,1)==Qe(1))&&(e(ne,2)==Qe(2)))||...
                                        ((e(ne,1)==Qe(2))&&(e(ne,2)==Qe(1)))
                                    %find local pt on which edge
                                    quadid = id; %quad id
                                    break %break k
                                end
                            end
                        end
                        end
                        if quadid>0 %exist neighbour quad
                            %find local pt id in quad(id)
                            %for a bdy pt: i=1,N j=1,N
                            if i~=1 && i~=N
                                ids = [i,1;N-i+1,1;i,N;N-i+1,N;...
                                    1,i;1,N-i+1;N,i;N,N-i+1];
                                %possible local pt id in quad(id)
                            elseif j~=1 && j~=N
                                ids = [j,1;N-j+1,1;j,N;N-j+1,N;...
                                    1,j;1,N-j+1;N,j;N,N-j+1];
                            else %corner pt
                                ids = [1,1;1,N;N,1;N,N];
                            end
                            dmin = Inf; %record min d
                            local_pid = 0;
                            for l = 1:size(ids,1)
                                temp_pid = ids(l,1)+(ids(l,2)-1)*N;
                                temp_p = Qnew(quadid).P(temp_pid,:);
                                d = norm(p(pid,:)-temp_p);
                                if d<dmin
                                    dmin = d;
                                    local_pid = temp_pid;
                                end
                            end
                            temp = ones(size(Qnew(quadid).C{local_pid}))*quadid;
                            nei_ids2 = [temp,Qnew(quadid).C{local_pid}];
                            %neighbour pt ids in quad(quadid)
                            nei_ps{eid,pid} = [nei_ps{eid,pid};nei_ids2];
                            dup{eid,pid} = [dup{eid,pid};[quadid,local_pid]];
                        end
                    end
                end
            end
            %until now find all neighbour pts ids
        end
    end
end
%find all neighbour quads
nei_qs = cell(Ne,N^2); %record all nei_quads, Nx2, [eid local_qid]
for i = 1:Ne
    for j = 1:N^2
        nei_qs{i,j} = [i*ones(size(Qnew(i).QE{j})),Qnew(i).QE{j}]; %local quads
        dup_pts = dup{i,j};
        if size(dup_pts,1)>0 %exist dup pts
            for k = 1:size(dup_pts,1)
                eid = dup_pts(k,1);
                pid = dup_pts(k,2);
                nei_qs{i,j} = [nei_qs{i,j};...
                    [eid*ones(size(Qnew(eid).QE{pid})),Qnew(eid).QE{pid}]];
            end
        end
    end
end