function SL = propagateSL(SL,S,TR,N,Sid,e,d0,fh,N_S0,SLcopy)
%propagate along ONE SL
%INPUT:
%SL: one streamline, SLcopy(i,j)
%S: combined singularities
%TR: triangle mesh
%N: nodes
%Sid: the ith singularity point
%e: boundary edges
%d0: grid size
%N_S0: number of boundary singularities, corresponds to row num of SL0
%SLcopy: all streamlines
status = zeros(1,size(TR.ConnectivityList,1)); %status of each triangle
for i = 1:size(TR.ConnectivityList,1)
    %initialize:x,y,eid,vid1,vid2,status(i)
    if size(SL.x,1)==1 %the initial point
        d = SL.d0; %initial direction
        if Sid<=N_S0 %boundary singularity
            eid = pointLocation(TR,SL.x+1e-6.*d);
            %cannot use S.E_ID, because d is different
        else
            eid = S(Sid).E_ID; %starting triangle ID
        end
        status(eid) = true; %record checked triangle ID
        %find vertex ID given a point in a triangle
        [x,y,vid1,vid2] = find_first_vid(S(Sid),eid,d,TR);
        SL.x(end+1,:) = [x y]; %add first intersection point
        %if not the initial point
        %eid,vid1,vid2 has already been calculated from previous step
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE1:REACH BOUNDARY
    %%%%%%%%%%%%%%%%%%%%%%%%%
    Lia1 = ismember([vid1 vid2],e,'rows');
    Lia2 = ismember([vid2 vid1],e,'rows');
    if Lia1 || Lia2 %edge is on boundary
        SL.end_type = 0; %ends on boundary
        SL.end_edge = [vid1 vid2]; %store end edge
        break
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE2:ANOTHER SINGULARITY POINT
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %check this before spiral, because goes back to another singularity
    %point does not counted as spiral
    if size(SL.x,1)==2 %the initial point & first intersection
        eid = find_eid(vid1,vid2,status,TR); %find next triangle id
    end
    break_status = 0;
    for j = 1:size(S,2)%find the S inside circle d
        tol = min(d0*fh([x y]),d0*fh([S(j).x S(j).y])); %min d0
        %tol = 2*max(d0*fh([x y]),d0*fh([S(j).x S(j).y])); %min d0
        if j~=Sid && sqrt((x-S(j).x)^2+(y-S(j).y)^2)<tol...%exclude Si itself
                && S(j).k>-4 %cannot connected to S with k=-4  
            %remove parallel SL within distance d0
            theta_min = pi; %find nearest direction starting from S
            for k = 1:size(SLcopy,2)
                if SLcopy(j,k).status %exists SL
                        d1 = SLcopy(j,k).d0; %direction starting from S
                        theta = acos(dot(-d,d1)/(norm(-d)*norm(d1)));
                        if theta<theta_min
                            theta_min = theta;
                            theta_id = k;%record SL id
                        end
                end
            end
            if theta_min<pi/10 %can connect to each other
            %if theta_min<pi/5 %can connect to each other
                SL.x(end,:) = 0.5*(([S(j).x S(j).y]+...
                    d0*fh([S(j).x S(j).y])*0.5*SLcopy(j,theta_id).d0)...
                    +SL.x(end,:));%one step smooth, |SL.d0|=1
                SL.x(end+1,:) = [S(j).x S(j).y]; %end with another singularity point
                %SLcopy(j,theta_id).status = 0; %remove correspond SL
                break_status = 1;
                SL.end_type = [j,theta_id]; %store end singularity id and direction
                break
            end
                
        end
    end
    if break_status
        break %break propagation
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %CASE3:SPIRAL
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %goes back to a triangle which has already been passed
    ti = edgeAttachments(TR,vid1,vid2); %triangle attached to v1,v2
    %should be 2 edges, becouse bdy has already been checked
    Eid1 = ti{1}(1); %ti: cell
    Eid2 = ti{1}(2);
    if status(Eid1) && status(Eid2) %both have been passed
        SL.end_type = -1;
        break
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %AFTER CHECK CRITERIA
    %%%%%%%%%%%%%%%%%%%%%%%%%
    %x: intersection point on an edge
    %d: direction to next triangle
    %eid: current triangle id
    %vid1,vid2: vertex ids of edge which has x on it
    %status(eid): stauts of the present triangle is true, update first
    %find the first intersection direction
    dt1 = find_nearest_d(x,y,d,vid1,vid2,N);
    %find the first intersection point
    [xt1,yt1,vid1t1,vid2t1,eid] = update(x,y,dt1,eid,vid1,vid2,TR,N);
    %find the second intersection direction
    %the nearest direction is RELATED to d
    dt2 = find_nearest_d(xt1,yt1,d,vid1t1,vid2t1,N);
    %update d
    d = 0.5*(dt1+dt2); %norm dt1/dt2/d=1
    %update x,y,vid1,vid2
    [x,y,vid1,vid2,eid] = update(x,y,d,eid,vid1,vid2,TR,N);
    %update eid    
    status(eid) = true; %update status of present triangle
    eid = find_eid(vid1,vid2,status,TR); %find next triangle id
    SL.x(end+1,:) = [x y];
end



