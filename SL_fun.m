function x = SL_fun(SL,r)
%function to describe streamline
%INPUTS:
%SL: streamline
%r: parameter(distance)
%OUTPUT:
%x: location on SL given r

n = size(SL.x,1); %number of points on SL
alpha = zeros(n,1);%ratio of distance from each pt to start pt
for i = 2:n
    alpha(i) = alpha(i-1)+norm(SL.x(i,:)-SL.x(i-1,:));
end
alpha = alpha/alpha(n);
for i = 2:n
    if alpha(i-1)<=r && r<=alpha(i) 
        beta = (r-alpha(i-1))/(alpha(i)-alpha(i-1));
        x = SL.x(i-1,:)*(1-beta)+SL.x(i,:)*beta;
        break
    end
end


