function p = mid_point(p1,p2,p3)
%find mid point using equal space method, SOR
%INPUT:
%p1,p2,p3: ordered point coordinates
%l0: mid length of previous step
%OUTPUT:
%p: mid point coordinate

%w = 1.995; %SOR factor
l1 = norm(p2-p1);
l2 = norm(p3-p2);
%l = (1-w)*l0+0.5*w*(l1+l2);
l = 0.5*(l1+l2);
if l<l1
    s = l/l1;
    p = (1-s)*p1+s*p2;
else
    s = (l-l1)/l2;
    p = (1-s)*p2+s*p3;
end
