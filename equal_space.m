function pnew = equal_space(Q,nei_ps,nei_qs,eid,pid,dup)
%find new pt location using equal-space method
%only apply for interier nodes
%INPUT:
%Q: Quad class
%nei_ps: neibour pts of p, [eid,qid]
%nei_qs: neibour quads of p, [eid,qid], include the quad contains p
%eid,pid: element/point id of p
%dup: duplicated pts 
%OUTPUT:
%pnew: new location after moving

%count number of neighbour quads
q_num = size(nei_qs,1);
if q_num ~= 4 
    %find the geometric center
    pnew = geo_center(Q,nei_ps,Q(eid).P(pid,:));
else
    %apply equal-space smooth
    p = zeros(9,2); %store 9 pts in a stencil
    p0 = Q(eid).P(pid,:); %pt need to be moved  
    status = ones(4,1); %check status of four neighbour quads
    for loop = 1:4 %find four nei_quads ccw
        for i = 1:4 %loop through four neighbour quads
            if status(i) %have not been checked before
                for j = 1:4 %four pts in a quad
                    %eid,pid of four quad pts
                    eid1 = nei_qs(i,1);
                    qid1 = nei_qs(i,2);
                    pid1 = Q(eid1).Q(qid1,j);
                    if ((eid1==eid)&&(pid1==pid)) || ...
                            member_check_row([eid1,pid1],dup{eid,pid},1e-10)
                        %find p in quad
                        status(i) = 0; %quad(i) been checked
                        break %j
                    end
                end
                if ~status(i) %been checked
                    break %i
                end
            end
        end %i
        if loop == 1
            for k = 1:3 %find following three pts ccw
                id = j+k; %local id in Q.Q
                if id>4
                    id = id-4;
                end
                pid1 = Q(eid1).Q(qid1,id);
                p(loop+k-1,:) = Q(eid1).P(pid1,:);
            end
        else
            for k = 1:2 %find following two pts ccw
                id = j+k; %local id in Q.Q
                if id>4
                    id = id-4;
                end
                pid1 = Q(eid1).Q(qid1,id);
                p(2*loop+k-1,:) = Q(eid1).P(pid1,:);
            end
        end
        %update eid,pid
        eid = eid1;
        pid = pid1;
    end %loop
    p(9,:) = p0; %pt need to be moved    
    r1 = mid_point(p(6,:),p(7,:),p(8,:));
    r2 = mid_point(p(5,:),p(9,:),p(1,:));
    r3 = mid_point(p(4,:),p(3,:),p(2,:));
    c1 = mid_point(p(4,:),p(5,:),p(6,:));
    c2 = mid_point(p(3,:),p(9,:),p(7,:));
    c3 = mid_point(p(2,:),p(1,:),p(8,:));
    p1 = mid_point(r1,r2,r3);
    p2 = mid_point(c1,c2,c3);
    pnew= 0.5*(p1+p2);
end


    
    
    