function y = curve12_1_y(x)
%curve function of example 12, rectangle with hole
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
y = polycurve_y(x,s_bdy);
    



