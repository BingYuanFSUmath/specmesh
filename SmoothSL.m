function SL = SmoothSL(SL,S,fd,fh,d0)
%Use Fourier filter to smooth the streamline
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        if SL(i,j).status %exists SL
            s1 = [S(i).x,S(i).y];
            if size(SL(i,j).x,1)>3
                s2 = SL(i,j).x(4,:);
                if fd(s1)<d0*fh(s1) || fd(s2)<d0*fh(s2)
                    %SL along and close to bdy, will not smooth 
                    continue
                end
            end
            Sid = SL(i,j).end_type(1);
            if Sid>0 %end with S
                s1 = [S(Sid).x,S(Sid).y];
                if size(SL(i,j).x,1)>3
                    s2 = SL(i,j).x(end-3,:);
                    if fd(s1)<d0*fh(s1) || fd(s2)<d0*fh(s2)
                        continue
                    end
                end
            end
            for k = 2:size(SL(i,j).x,1)-1 %loop through x
                %Fourier filter
                SL(i,j).x(k,:) = 0.25*(SL(i,j).x(k-1,:)...
                    +SL(i,j).x(k+1,:))+0.5*SL(i,j).x(k,:);% ad hoc update
            end
        end
    end
end
end