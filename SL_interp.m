function x = SL_interp(SL1,SL2)
%interpolate 2 streamlines start/end with same points
%INPUT:
%SL1/SL2: streamlines, opposite start/end points
%OUTPUT:
%x: nx2 matrix to store interpolation points locations

n = ceil(0.5*(size(SL1.x,1)+size(SL2.x,1))); %number of interpolation pts
x = zeros(n,2); %initialization
for i = 1:n
    s = (i-1)/(n-1); %parameter
    x(i,:) = SL_fun(SL1,s)*(1-s)+SL_fun(SL2,1-s)*s;
end