function y = curve6_1_y(x)
 pt = [-4 -4;4 -4;4 4;-4 4;-4 -4];
 y = polycurve_y(x,pt);