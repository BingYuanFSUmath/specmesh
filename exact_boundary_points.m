function [SLcopy,p_bdy] = exact_boundary_points(SLcopy,S0,p,ex)
%find all exact boundary pts on SLs
%OUTPUT:
%SLcopy: updated exact bdy pts on SLs
%p_bdy: store bdy S and bdy pts on SLs

p_bdy(size(S0,2)) = pBoundary;
for i = 1:size(S0,2)
    p_bdy(i).x = [[S0(i).x]' [S0(i).y]']; %store bdy singularities
end
%USE curve_t TO FIND t GIVEN [x y]
%STORE CURVE ID & t
for i = 1:size(p_bdy,2)
    for j = 1:ex.curve_num
        t = find_t(ex.curve(j),p_bdy(i).x);
        if t >= 0 %on this curve
            p_bdy(i).curve_id = j;
            p_bdy(i).t = t;
            break
        end
    end
    %does not need this part since bdy S are fixed
    if t<0 %can not find t
        [k,t] = linear_search(ex,p_bdy(i).x);
        p_bdy(i).curve_id = k;
        p_bdy(i).t = t;
    end
end

for i = 1:size(SLcopy,1)
    for j = 1:size(SLcopy,2)
        if SLcopy(i,j).status && SLcopy(i,j).end_type(1)==0
            %exist SL and ends on boundary
            %find exact end points on boundary curve
            p_bdy(end+1) = pBoundary; %add one point
            x1 = SLcopy(i,j).x(end,1);
            y1 = SLcopy(i,j).x(end,2);
            x2 = SLcopy(i,j).x(end-1,1);
            y2 = SLcopy(i,j).x(end-1,2);
            p1 = p(SLcopy(i,j).end_edge(1),:); %edge point on boundary
            p2 = p(SLcopy(i,j).end_edge(2),:);
            for k = 1:ex.curve_num
                t = find_t(ex.curve(k),p1);
                if t >= 0 %on this curve
                    p_bdy(end).curve_id = k;
                    break
                end
            end
            %prolem often caused by tol in find_t
            %since p1/p2 are bdy pts
            %can relax tol
            t1 = t; %parameter of two edge pts
            t2 = find_t(ex.curve(k),p2);
            if (t1<0 || t2<0) %can not find t
                [k,t] = linear_search(ex,SLcopy(i,j).x(end,:));
                p_bdy(end).curve_id = k;
            else
            tmin = min([t1 t2]);
            tmax = max([t1 t2]);
            theta0 = atan2(y1-y2,x1-x2); %angle of SL, [-pi,pi]
            f = @(t) root_finding_f(t,theta0,ex,k,x2,y2);
            if abs(tmax-tmin) > 1-(tmax-tmin) %t crosses 0/1
                if f(tmin)*f(0) <= 0
                    t = fzero(f,[0 tmin]);
                else
                    t = fzero(f,[tmax 1]);
                end
            else
                if (f(tmin)*f(tmax)>0)
                    error('Error. Can not find range of t.')
                end
                t = fzero(f,[tmin tmax]);
            end
            end
            p_bdy(end).t = t;
            p_bdy(end).x = position_at(ex.curve(k),t);
            SLcopy(i,j).x(end,:) = p_bdy(end).x; %replace end point
        end
    end
end
%remove p_bdy(1) if S0 is empty
if S0(1).E_ID == 0 %no S0
    p_bdy(1) = [];
end



function [num,t0] = linear_search(ex,p,varargin)
%find t, given p=[x y]
%INPUT:
%ex:Example class
%p:pt on curve
%varargin: curve number, if known
%OUTPUT:
%num: curve number
%t0: parameter at p

dt = 1e-5; %line search step for parameter t
d0 = Inf;
k1 = 1;
k2 = ex.curve_num;
if (~isempty(varargin))
    k1 = varargin{1};
    k2 = varargin{1};
end
for k = k1:k2        
    str = ['@' ex.curve(k).name '_p'];
    fp = str2func(str);
    dmin = Inf;
    for t = 0:dt:1
        p0 = fp(t);
        d = norm(p0-p);
        if d<dmin
            tmin = t;
            dmin = d;
        end
    end
    if dmin<d0
        d0 = dmin;
        t0 = tmin;
        num = k;
    end
end
