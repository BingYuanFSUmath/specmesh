function N = acute(N,e,S0_id,r)
%Modify actue angle by adding pi, then interpolate along each side
%INPUT: 
%N: nodes
%e: boundary edges
%S0_id: %store S0/neighbour ids and angle
%r: ratio of points taken on each side
%OUTPUT:
%N: nodes with modified N.d/cross
SL(size(S0_id,1),2) = Streamline; %store steamlines, 2 for each S0
for i = 1:size(S0_id,1) %loop through S0
    if S0_id(i,4)<pi/4+1e-4 %acute angle 
        SL(i,1).d0 = S0_id(i,1);%starting point id
        SL(i,2).d0 = S0_id(i,1);%starting point id
        SL(i,1).d0(end+1) = S0_id(i,2);%second point id
        SL(i,2).d0(end+1) = S0_id(i,3);%second point id
        for j = 1:2
            end_point = 1; %check end point
            while end_point
                for k = 1:size(e,1) %find next point id
                    if SL(i,j).d0(end) == e(k,1) && ~ismember(e(k,2),SL(i,j).d0)
                        SL(i,j).d0(end+1) = e(k,2);
                        break %break k
                    end
                    if SL(i,j).d0(end) == e(k,2) && ~ismember(e(k,1),SL(i,j).d0)
                        SL(i,j).d0(end+1) = e(k,1);
                        break
                    end
                end
                if ismember(SL(i,j).d0(end),S0_id(:,1))
                    end_point = 0; %reach another S0
                end
            end
        end
    end  
end
%finish finding SL

%Interpolate r ratio of SL
SLnew(size(SL,1),size(SL,2)) = Streamline;%SLnew copy half of SL
for i = 1:size(SL,1)
    for j = 1:size(SL,2)
        n = size(SL(i,j).d0,2);%d0: row array
        if n %exists SL
            n = ceil(n*r); %r ratio points
            SLnew(i,j).d0 = SL(i,j).d0(1:n);
        end
    end
end
SL = SLnew;

for i = 1:size(S0_id,1)
    if size(SL(i,1).d0,1) %exists SL
        %find left and right side
        v21 = [N(S0_id(i,2)).x-N(S0_id(i,1)).x N(S0_id(i,2)).y-N(S0_id(i,1)).y];
        v31 = [N(S0_id(i,3)).x-N(S0_id(i,1)).x N(S0_id(i,3)).y-N(S0_id(i,1)).y];
        theta21 = atan2(v21(2),v21(1)); %[-pi,pi]
        theta31 = atan2(v31(2),v31(1));
        if theta21<theta31 %left side has smaller angle
            Left = 1; %first SL
        else
            Left = 2; %second SL
        end
        if theta21*theta31<0 %across x-axis
            if v21(1)>0 %opens to right
                if theta21<0
                    Left = 1;
                else
                    Left = 2;
                end
            else %opens to left
                if theta21>0
                    Left = 1;
                else
                    Left = 2;
                end
            end
        end
        for j = 1:2
            alpha = zeros(size(SL(i,j).d0)); %interpolate ratio
            for k = 2:size(SL(i,j).d0,2)
                x = N(SL(i,j).d0(k)).x-N(SL(i,j).d0(k-1)).x;
                y = N(SL(i,j).d0(k)).y-N(SL(i,j).d0(k-1)).y;
                alpha(k) = alpha(k-1)+norm([x y]);
            end
            alpha = alpha/alpha(end);
            if j == Left %-pi on left edge
                for k = 1:size(SL(i,j).d0,2)
                    N(SL(i,j).d0(k)).d = ...
                        mod(N(SL(i,j).d0(k)).d-(1-alpha(k))*pi,2*pi);
                end
            else %+pi on right edge
                for k = 2:size(SL(i,j).d0,2)%don't +pi at S0
                    N(SL(i,j).d0(k)).d = ...
                        mod(N(SL(i,j).d0(k)).d+(1-alpha(k))*pi,2*pi);
                end
            end
        end
        %if singularity appear at S0, +pi at S0
        dtheta12 = N(S0_id(i,2)).d-N(S0_id(i,1)).d; %find dtheta
        dtheta12 = mod(dtheta12+pi,2*pi)-pi;
        dtheta23 = N(S0_id(i,3)).d-N(S0_id(i,2)).d; %find dtheta
        dtheta23 = mod(dtheta23+pi,2*pi)-pi;
        dtheta31 = N(S0_id(i,1)).d-N(S0_id(i,3)).d; %find dtheta
        dtheta31 = mod(dtheta31+pi,2*pi)-pi;
        if (dtheta12<0 && dtheta23<0 && dtheta31<0) ...
                ||(dtheta12>0 && dtheta23>0 && dtheta31>0)
            %singularity point
            N(S0_id(i,1)).d = mod(N(S0_id(i,1)).d+pi,2*pi);%add pi at S0
        end
    end
end


            
            
            
            
            
            
            
            
            
            
            
            
            
            
