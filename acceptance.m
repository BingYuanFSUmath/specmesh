function acc = acceptance(beta,beta_new,quad_ids,method)
%Acceptance Criteria for Constrained Laplacian Node Movement
%INPUT:
%beta: old metric
%beta_new: new metric
%quad_ids: neighbour quad ids
%method: acceptance criteria method
%OUTPUT:
%acc: acceptance

switch method
    case 1
        acc = 1; %no constrain
    case 2
        acc = acceptance1(beta,beta_new,quad_ids); %avoid bad quad
    case 3
        acc = acceptance2(beta,beta_new,quad_ids); %ANSYS method
end


    
    
    
    