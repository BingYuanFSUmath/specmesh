classdef AdaptCurveInterpChild
    properties
        glNodes
        baryWeights
        values
        child_left
        child_right
    end
    methods
        function self = AdaptCurveInterpChild
            self.glNodes = []; 
            self.baryWeights = [];
            self.values = [];
            self.child_left = [];
            self.child_right = [];
        end
        function [self,locations] = init_child(self,...
                glNodes,baryWeights,...
                interval,curve,tol,locations)
            xpts = zeros(size(glNodes,1),2);
            self.glNodes = glNodes;
            if interval(1) > locations(end)
                locations(end+1) = interval(1);
            end
            %see if single interpolant is good enough
            N = size(glNodes,1);
            for j = 1:N
                t = interval(1)+0.5*(self.glNodes(j)+1)*...
                    (interval(2)-interval(1));
                t = mod(t,1);
                xpts(j,:) = curve.position_at(t);
            end
            e_max = max_interp_error(self.glNodes,xpts,...
                baryWeights,curve,interval);
            if e_max <= tol
                self.baryWeights = baryWeights;
                self.values = xpts;
            else
                %sundivide the interval
                new_interval = zeros(2,1);
                new_interval(1) = interval(1);
                new_interval(2) = interval(1)+0.5*...
                    (interval(2)-interval(1));
                self.child_left = AdaptCurveInterpChild;
                [self.child_left,locations] = ...
                    self.child_left.init_child...
                    (glNodes,baryWeights,new_interval,...
                    curve,tol,locations);
                new_interval(1) = new_interval(2);
                new_interval(2) = interval(2);
                self.child_right = AdaptCurveInterpChild;
                [self.child_right,locations] = ...
                    self.child_right.init_child...
                    (glNodes,baryWeights,new_interval,...
                    curve,tol,locations);
            end
        end
        %{
        function x = child_position_at(self,xi) %recursive
            x = zeros(1,2);
            N = size(self.baryWeights,1);
            if size(self.child_left,1) == 0 %not associated
                for j = 1:2
                    x(j) = LagrangeInterp(xi,N,slef.glNodes,...
                        self.values(:,j),self.baryWeights);
                end
            else
                if xi <= 0
                    eta = 1+2*xi;
                    x = child_position_at(self.child_left,eta);
                else
                    eta = -1+2*xi;
                    x = child_position_at(self.child_right,eta);
                end
            end
        end
        %}
    end
end