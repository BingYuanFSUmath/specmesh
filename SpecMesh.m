%CrossField 2D quad mesh generator
clc
clearvars
%Example IDs:
%1.Polygon
%2.Half circle
%3.Circle
%4.Rectangle with hole
%5:Four circles inside ellipse with various r
%6:Square inside square

%1. Set Parameters
prompt = {'Enter Example Id:','Order of Polynomial:','Error Tolerance:'};
dlg_title = 'Set Parameters';
num_lines = 1;
defaultans = {'39','7','1e-3'};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

exampleID = str2double(answer{1}); 
N_poly = str2double(answer{2}); %order of interpolation polynomials
Tol_poly = str2double(answer{3}); %tolerance of interpolation error
r = 0.5; %interpolation ratio of acute angle

%2. Generate Base Trimesh
[s_bdy,p,t,e,ex] = trimesh_generator(exampleID);
%s_bdy: Boundary singularity points(pts where bdy is not continuous)
%p: points coordinates
%t: triangle vertex pt ids
%e: boundary edges
%ex: example class
d0 = ex.d0; %min grid size
fd = ex.fd; %distance function, -:inside, +:outside
fh = ex.fh; %scaled edge length function
  
%3. Initialization
TR = triangulation(t,p);%generate triangulation class
[N,S0,SL0,S0_id] = initialize_N(s_bdy,p,t,e,TR);
if size(S0_id,1)%exists boundary singularity S0
    N = acute(N,e,S0_id,r);%modify acute angles
end

%4. Solve cross field using FEM
U = fem(p,t,e,N);%complex solution, represent vector(angle does not work)
U = atan2(imag(U),real(U));%convert to angle [-pi,pi]
for i = 1:size(N,1)
    N(i).cross = U(i)/4;%cross field
    N(i).d = mod(U(i),2*pi);%[0 2pi]
end

%5. Find singularity points
S = Singularity;
S = find_S_new(S,N,TR);
S = S(1,2:end); %remove the initial zero
S = collide_S(S,d0,fh);
plot_vector_field(N,S,TR,exampleID)

%6. Initialize streamlines
SL = Streamline;
if size(S,2) %initialize SL when S exists
    SL = SL.initializeSL(S,TR,N);%find starting directions
end
Scopy = [S0 S]; %combine bdy and inside singularities
%combine SLs
SLcopy(size(SL0,1)+size(SL,1),max(size(SL0,2),size(SL,2))) = Streamline;
SLcopy(1:size(SL0,1),1:size(SL0,2)) = SL0;
SLcopy(size(SL0,1)+1:end,1:size(SL,2)) = SL;

 
%7. Propagate streamlines
N_S0 = size(S0,2); %number of bounday singularities
for i = 1:size(SLcopy,1) %propagate each streamline
    for j = 1:size(SLcopy,2) %column num i is Sid
        if SLcopy(i,j).status%only propagate exist SL
            SLcopy(i,j) = propagateSL(SLcopy(i,j),Scopy,TR,N,i,e,d0,fh,N_S0,SLcopy);
        end
    end
end

%8. Cleanups
d0=2*d0;
%smooth first to avoid great direction change on a SL
for i = 1:5 %smooth N times
    SLcopy = SmoothSL(SLcopy,Scopy,fd,fh,d0);
end
%Remove duplicated SLs
SLcopy = RemoveSL_new(SLcopy,Scopy,TR,N,e,d0,fh,N_S0);
plot_mesh(N,S,SLcopy,e,exampleID)

%9. Subdivide SLs
[SLnew,Quad] = SubdivideSL(SLcopy,S0,ex,N_poly,Tol_poly,TR,N,p,e,d0,fh,fd);
plot_mesh(N,S,SLnew,e,exampleID)

%10. Generate quad mesh
%find inner intersection pts
Inter_p = IntersectionPt(SLnew,d0,fh,ex.box);
%generate Quad class
Quad = Quad.generator(SLnew,Inter_p);

%11. Adjust invalid quads
Quad = Invalid_adjust(Quad,ex);

%12. Smoothing
%Quad = Subdivide_smooth_new(Quad,N_poly,ex,s_bdy);

