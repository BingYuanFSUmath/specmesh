function [x,w] = LegendreLobattoNandW(N)
%find Legendre-Gauss-Lobatto quadrature nodes and weights
%INPUT:
%N: num of nodes
%OUTPUT
%x: abscissa, column vector, [-1,1]
%w: weights, column vector

P = @LegendrePoly;
dP = @LegendrePolyDeri;
syms t
x = double(vpasolve(dP(N-1,t)==0,t)); 
%convert result to double precision variable
w = zeros(N,1);
w(1) = 2/(N*(N-1));
w(N) = w(1);
for i = 2:N-1
    w(i) = 2/(N*(N-1)*P(N-1,x(i))^2);
end