function x0 = bisection(f,x,tol)
%bisection function
%INPUT:
%f: function handle
%x: bisection interval, [a,b]
%tol: tolerance to end bisection
%OUTPUT:
%x0: zero within tolerance

a = x(1);
b = x(2);
while 1
    if abs(f(a))<tol
        x0 = a;
        break
    elseif abs(f(b))<tol
        x0 = b;
        break
    elseif f(a)*f(b)>0
        error('Error. f(a) f(b) must have different sign.')
    else
        c = 0.5*(a+b);
        if f(a)*f(c)<0
            b = c;
        else
            a = c;
        end
    end
end
