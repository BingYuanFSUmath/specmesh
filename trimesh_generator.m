function [s_bdy,p,t,e,ex] = trimesh_generator(exampleID)
%examples of trimesh
%INPUT:
%exampleID: example id
%OUTPUT:
%s_bdy: boundary singularity coordinates, first pt duplicated
%p/t/e: points/triangle indices/boundary edges
%ex: example class with d0,fd,fh,box

%1.Polygon
if exampleID == 1
    curve_num = 1;
    s_bdy=[0 -1;0.5 0;-0.1 1;-1 0;-0.2 -0.2;0 -1];
    d0 = 0.1;
    fd = @(p) dpoly(p,s_bdy);
    fh = @(p) huniform(p);
    box = [-1,-1; 1,1];
    %poly_generator(exampleID,1,s_bdy);
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e = boundedges(p,t);
end

%2.Half circle
if exampleID == 2
    curve_num = 1;
    s_bdy=[-1 0;1 0;-1 0];
    d0 = 0.1;
    fd=@(p) ddiff(dcircle(p,0,0,1),drectangle(p,-1,1,-1,0));
    fh = @huniform;
    box = [-1,0;1,1]; 
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end

%3.Circle
if exampleID == 3
    curve_num = 1;
    s_bdy=[];
    d0 = 0.1;
    fd=@(p) sqrt(sum(p.^2,2))-1;%distance function of circle
    fh = @huniform;
    box = [-1,-1;1,1];
    %ellipse_generator(exampleID,1,1,1,0,0);
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end

%4.Rectangle with hole
if exampleID == 4
    curve_num = 2;
    s_bdy=[-1 -1;-1 1;1 1;1 -1;-1 -1];
    d0 = 0.1;
    fd=@(p) ddiff(drectangle(p,-1,1,-1,1),dcircle(p,0,0,0.5));
    fh = @huniform;
    box = [-1,-1;1,1];
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e = boundedges(p,t);
end

%5:four circles inside ellipse with various r
if exampleID == 5
    curve_num = 5;
    d0 = 0.05;
    s_bdy=[];
    fd=@(p) ddiff(p(:,1).^2/2^2+p(:,2).^2/1^2-1,...
        dunion(dunion(dunion(dcircle(p,0,0.5,0.3),dcircle(p,1,0,0.2)),...
        dcircle(p,-1,0,0.5)),dcircle(p,0.5,-0.5,0.15)));
    fh=@(p) min(min(dcircle(p,0,0.5,0),min(dcircle(p,1,0,0),...
        min(dcircle(p,-1,0,0),dcircle(p,0.5,-0.5,0))))/0.15,0.5/0.15);
    box = [-2,-1;2,1];
    %ellipse_generator(exampleID,1,2,1,0,0);
    %ellipse_generator(exampleID,2,0.3,0.3,0,0.5);
    %ellipse_generator(exampleID,3,0.2,0.2,1,0);
    %ellipse_generator(exampleID,4,0.5,0.5,-1,0);
    %ellipse_generator(exampleID,5,0.15,0.15,0.5,-0.5);
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end

%6:Square inside square
if exampleID == 6
    curve_num = 2;
    d0 = 0.2;
    alpha = 0.35*pi; %rotation angle
    a = 1.13; %inner square half length
    pv1 = [-4 -4;4 -4;4 4;-4 4;-4 -4];
    z = exp((alpha+pi/4)*1i)*a*[1+1i;-1+1i;-1-1i;1-1i;1+1i];
    %complex coordinates of inner square
    pv2 = [real(z) imag(z)];
    s_bdy=[pv1(1:end-1,:);pv2(1:end-1,:);pv1(1,:)];
    fd=@(p) ddiff(dpoly(p,pv1),dpoly(p,pv2));
    fh = @huniform;
    box = [-4,-4;4,4];
    %poly_generator(exampleID,1,pv1);
    %poly_generator(exampleID,2,pv2);
    [p,t]=distmesh2d_checked(fd,fh,d0,box,s_bdy);
    e=boundedges(p,t);
end

%39.Indian Ocean
if exampleID == 39
    curve_num = 3;
    d0 = 0.02; 
    load ocean_coarse_poly.mat
    fd = @(p) ddiff(dpoly(p,ocean{1}),dunion(dpoly(p,ocean{2}),dpoly(p,ocean{3})));
    fh = @(p) -fd(p)+0.2;
    box = [-4.5,-3.5;1,-1];
    load ocean_coarse_poly_trimesh.mat
    e = boundedges(p,t);%generate boundary edges
end 

ex = Example(exampleID,curve_num);
ex.d0= d0;
ex.fd = fd;
ex.fh = fh;
ex.box = box;




