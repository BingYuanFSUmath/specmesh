function pnew = geo_center(Qnew,nei_ps,p0)
%find the geometric center 
%remove duplicated neigbour pts
nei_pts = ones(size(nei_ps));
for l = 1:size(nei_ps,1)
    nei_pts(l,:) = Qnew(nei_ps(l,1)).P(nei_ps(l,2),:);
end
nei_pts = unique_row(nei_pts,1e-10);
%sort point in ccw order
p = sort_ccw(nei_pts,p0);
%find geometric center
[geom, ~, ~] = polygeom(p(:,1),p(:,2));
pnew = [geom(2),geom(3)];


function p = sort_ccw(p,p0)
%sort p counterclockwise
n = size(p,1);
theta = zeros(n,1);
for i = 1:n
    v = p(i,:)-p0;
    theta(i) = atan2(v(2),v(1)); %-pi~pi
end
p_sort = [p,theta];
p_sort = sortrows(p_sort,3);
p = p_sort(:,1:2);