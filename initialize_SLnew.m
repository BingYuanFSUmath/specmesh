function [S,SLnew] = initialize_SLnew(S,SLnew,p,e,TR,fd)
%find minimum distance from S to edges
dmin = Inf;
edge_id = 0;
for i = 1:size(e,1)
    x0 = S.x;
    y0 = S.y;
    x1 = p(e(i,1),1);
    y1 = p(e(i,1),2);
    x2 = p(e(i,2),1);
    y2 = p(e(i,2),2);
    %intersection point on line
    a = y1-y2;
    b = x2-x1;
    c = x1*y2-y1*x2;
    x = (b*(b*x0-a*y0)-a*c)/(a^2+b^2);
    y = (a*(-b*x0+a*y0)-b*c)/(a^2+b^2);
    if x >= min(x1,x2) && x <= max(x1,x2) %intersect on edge
        %distance from point to line
        d = abs((x2-x1)*(y1-y0)-(x1-x0)*(y2-y1))/sqrt((x2-x1)^2+(y2-y1)^2);
        if d < dmin
            dmin = d;
            edge_id = i;
            x_edge = x;
            y_edge = y;
        end
    end
end
%find intersection edge and point
SLnew.x = [x_edge y_edge];
SLnew.status = true;
%propagate direction: normal vector at S
%use normal vector on edge to approximate
p1 = p(e(edge_id,1),:); %edge pts
p2 = p(e(edge_id,2),:);
v = p2-p1; %tangent vector
d0 = [-v(2),v(1)]; %normal vector
temp1 = [x_edge,y_edge]+d0*0.01;
temp2 = [x_edge,y_edge]-d0*0.01;
if fd(temp2)<fd(temp1) %temp2 is inside domain
    d0 = -d0;
    temp = temp2;
else %temp1 is inside domain
    temp = temp1;
end
%{
%fd is not accurate,1e-10 may cause problem
if fd(temp)>1e-10 %edge outside domain
    d0 = -d0; %point inward
    temp = [x_edge,y_edge]+d0*0.01;%update temp
end
%}
SLnew.d0 = d0/norm(d0);
%---------------------------
%won't give exact S location
S.x = x_edge;
S.y = y_edge;
%---------------------------
S.k = -3; %one streamline
%find triangle id which contain this edge
if norm([S.x,S.y]-p1)<1e-5 || norm([S.x,S.y]-p2)<1e-5
    %S is a end pt on edge
    %find triangles contains temp
    id = pointLocation(TR,temp);
    %---------------------------
    %to be more accurate, find connected triagles, find intersection
    %point on each edge, if intersection pt is on edge and not the S
    %---------------------------
else
    for i = 1:size(TR,1)
        if ismember(e(edge_id,1),TR(i,:)) &&...
                ismember(e(edge_id,2),TR(i,:))
            id = i;
            break
        end
    end
end
S.E_ID = id;