function Qnew = subdivide_quad(Q,Ne,N,ex)

Qnew(Ne,1) = QuadClass;
for i = 1:Ne
    x = zeros(4,2); %four corner pts
    Qnew(i).P = zeros(N^2,2);
    Qnew(i).C = cell(N^2,1);
    Qnew(i).M = zeros(N^2,1); %flag of local pts on global boundary
    Qnew(i).QE = cell(N^2,1); %store local quads connected to p
    for j = 1:4
        x(j,:) = Q.P(Q.Q(i,j),:);
    end
    Gamma = cell(4,1); %store gamma functions
    nodes = Q.E_x(Q.QE(i,:));
    for j = 1:4 %each quad edge
        if Q.Q(i,j)<=Q.nbpts %mark boundary pt
            switch j
                case 1
                    pid = 1;
                case 2
                    pid = N;
                case 3
                    pid = N*N;
                otherwise
                    pid = (N-1)*N+1;
            end
            Qnew(i).M(pid) = 1;
        end
        if j==1 || j==2
            if Q.E(Q.QE(i,j),1) ~= Q.Q(i,j) %exchange
                nodes{j} = flipud(nodes{j});
            end
        end
        if j==3 || j==4
            if Q.E(Q.QE(i,j),1) == Q.Q(i,j) %exchange
                nodes{j} = flipud(nodes{j});
            end
        end
        Gamma{j} = @(t) piecewise_linear(nodes{j},t);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %use exact bdy for bdy edges
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %2 bdy pts ~= bdy edge
        if Q.QE(i,j)<=Q.nbpts
            %boundary edge
            for k = 1:ex.curve_num
                t1 = ex.curve(k).find_t(nodes{j}(1,:));
                t2 = ex.curve(k).find_t(nodes{j}(end,:));
                if t1>=0 && t2>=0
                    break
                end
            end
            if t1<0||t2<0
                error('Error. Can not find t.')
            end
            if t2-t1>1+t1-t2 %cross 0/1
                Gamma{j} = @(t) ex.curve(k).position_at(mod(t1-(1+t1-t2)*t+1,1));
            elseif t1-t2>1+t2-t1
                Gamma{j} = @(t) ex.curve(k).position_at(mod(t1+(1+t2-t1)*t,1));
            else
                Gamma{j} = @(t) ex.curve(k).position_at(t1+(t2-t1)*t);
            end
        end
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    end
    for m = 1:N
        for n = 1:N
            xi = 1.0/(N-1)*(n-1);
            eta = 1.0/(N-1)*(m-1);
            xy = (1-eta)*Gamma{1}(xi)+eta*Gamma{3}(xi)...
                +(1-xi)*Gamma{4}(eta)+xi*Gamma{2}(eta)...
                -x(1,:)*(1-xi)*(1-eta)-x(2,:)*xi*(1-eta)...
                -x(3,:)*eta*xi-x(4,:)*(1-xi)*eta;
            p_id = n+(m-1)*N;
            Qnew(i).P(p_id,:) = xy;
        end
    end
    %mark all bdy pts
    %can not use elseif, may be more than one bdy edge
    if Q.QE(i,1)<=Q.nbpts
        %e1(p1p2) is a bdy edge, local p(:,1) are on bdy
        jj = 1;
        for ii = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.QE(i,2)<=Q.nbpts
        %e2(p2p3) is a bdy edge, local p(N,:) are on bdy
        ii = N;
        for jj = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.QE(i,3)<=Q.nbpts
        %e3(p3p4) is a bdy edge, local p(:,N) are on bdy
        jj = N;
        for ii = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
    if Q.QE(i,4)<=Q.nbpts
        %e4(p4p1) is a bdy edge, local p(1,:) are on bdy
        ii = 1;
        for jj = 1:N
            Qnew(i).M(ii+(jj-1)*N) = 1;
        end
    end
end
%construct local quad connectivity
Qnew(1).Q = zeros((N-1)^2,4);
for qid = 1:(N-1)^2
    pid = ceil(qid/(N-1))-1+qid;
    Qnew(1).Q(qid,:) = [pid,pid+1,pid+1+N,pid+N];
end
%local quad connectivity is same for each element
for i = 2:Ne
    Qnew(i).Q = Qnew(1).Q;
end
%construct local point connectivity
for m = 1:N
    for n = 1:N
        p_id = n+(m-1)*N;
        %pts connected to p
        for ii = -1:1
            for jj = -1:1
                if (ii*jj==0) && (~(ii==0 && jj==0))
                    %i-1,j;i+1,j;i,j-1;i,j+1
                    nn = n+ii;
                    mm = m+jj;
                    if nn>0 && mm>0 && nn<=N && mm<=N
                        Qnew(1).C{p_id}(end+1,1) = nn+(mm-1)*N;
                    end
                end
            end
        end
        %local quads connected to p
        for ii = 1:(N-1)^2
            if ismember(p_id,Qnew(1).Q(ii,:))
                Qnew(1).QE{p_id}(end+1,1) = ii;
            end
        end
    end
end
%local pt connectivity is same for each element
for i = 2:Ne
    Qnew(i).C = Qnew(1).C;
    Qnew(i).QE = Qnew(1).QE;
end

%test plot for local pt id->global pt id
%{
for i = 1:Ne
    figure
    for m = 1:N
        for n = 1:N
            pid = n+(m-1)*N;
            scatter(Qnew(i).P(pid,1),Qnew(i).P(pid,2),'b')
            hold on
            text(Qnew(i).P(pid,1)+0.01,Qnew(i).P(pid,2)+0.01,num2str(pid))
            hold on
            text(Qnew(i).P(pid,1)-0.01,Qnew(i).P(pid,2)-0.01,...
                num2str([n,m]),'Color','red')
            hold on
        end
    end
    hold off
    axis equal
end
%}

%test plot for Q.M, bdy pts
%{
figure
for i = 1:Ne
    scatter(Qnew(i).P(Qnew(i).M==1,1),Qnew(i).P(Qnew(i).M==1,2),'r')
    hold on
end
hold off
axis equal
%}