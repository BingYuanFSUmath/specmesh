%parametric curve class
classdef pCurve
    properties
        id %id of the curve
        name %curve name, string, examplenumber_id
    end
    methods
        function curve = pCurve %construct curve
            curve.id = [];
            curve.name = [];
        end
        function curve = init_pCurve(curve,id,name) %initialize curve
            curve.id = id;
            curve.name = name;
        end
        function y = eval_y(curve,x) %evaluate y, given x
            %may return several y values
            str = ['@' curve.name '_y'];
            f = str2func(str);%call curve function by its name
            y = f(x);
        end
        function p = position_at(curve,t) %find [x y], given t
            str = ['@' curve.name '_p'];
            f = str2func(str);%call curve function by its name
            p = f(t);
        end
        function t = find_t(curve,p) %find t, given p=[x y]
            str = ['@' curve.name '_t'];
            f = str2func(str);%call curve function by its name
            t = f(p);
        end
    end
end