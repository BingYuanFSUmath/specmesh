%root finding function to find exact interpolation on curve
function f = root_finding_f(t,theta0,ex,k,x2,y2)

p = position_at(ex.curve(k),t);
f = atan2(p(2)-y2,p(1)-x2)-theta0;
if abs(f) > pi %at discontinuity angle(-pi/pi)
    f = f+sign(theta0)*2*pi;
end
