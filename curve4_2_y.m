function y = curve12_2_y(x)
%curve function of example 12, rectangle with hole
%INPUT:
%x: x coordinate
%OUTPUT:
%y: y coordinate, may be several, row vector

xmin = -0.5;
xmax = 0.5;
r = 0.5;
if x < xmin || x > xmax %out of region
    y = 1e10; %helps to find intersection using root finding method
else 
    y = [0 0];
    y(1) = sqrt(r^2-x^2);
    y(2) = -sqrt(r^2-x^2);
end
    



