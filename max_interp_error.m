function error = max_interp_error(nodes,values,w,curve,curve_interval)
%find max interpolation error using linear search
%INPUT:
%nodes: interpolation nodes
%values: interpolation points, Nx2
%w: interpolation weights
%curve: curve class
%curve_interval: [tmin,tmax], tmax could be >1
%OUTPUT:
%error: max interpolation error
m = 20; %divide the curve to 20 pieces
m0 = 100; %101 sample pts on interpolation
n = size(nodes,1); %interpolation pt number
dt = (curve_interval(2)-curve_interval(1))/m;
dxi = 2/m0;
x = zeros(m0+1,2); %record sample pts on interpolation
for j = 1:m0+1
    xi = -1+(j-1)*dxi;
    for k = 1:2
        x(j,k) = LagrangeInterp(xi,n,nodes,values(:,k),w);
    end
end
error = 0;
for j = 1:m+1
    t = curve_interval(1)+(j-1)*dt;
    t = mod(t,1);
    xcurve = curve.position_at(t);
    distVector = sqrt((x(:,1)-xcurve(1)).^2+(x(:,2)-xcurve(2)).^2);
    e = min(distVector);
    error = max(error,e);
end
end
