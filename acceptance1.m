function acc = acceptance1(beta,beta_new,quad_ids)
%Acceptance Criteria for Constrained Laplacian Node Movement


N = size(quad_ids,1); %number of neighbour quads
acc = 1;
bmin = 0.1; %acceptable quality
for i = 1:N
    eid = quad_ids(i,1);
    qid = quad_ids(i,2);
    b = beta_new(eid,qid); %new beta
    b0 = beta(eid,qid); %old beta
    if b<bmin %bad quad or convex/inverted quad
        if b<b0 % worsens significantly
            acc = 0;
            break
        elseif b>b0 %improves significantly
            acc = 1;
        end
    end
end
            
    
    
    
    